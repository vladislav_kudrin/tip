package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.input.MouseEvent;
import javafx.scene.Node;

/**
 * Controls sample.fxml.
 *
 * @author Vladislav
 * @version 1.0
 * @since 05/12/2020
 */
public class Controller {
    @FXML
    private double xOffset;

    @FXML
    private double yOffset;

    @FXML
    private ToggleGroup tip = new ToggleGroup();

    @FXML
    private ObservableList<String> currency = FXCollections.observableArrayList("USD",
            "EUR",
            "RUB");
    @FXML
    private TextField totalField;

    @FXML
    private RadioButton fiveTipRadioButton;

    @FXML
    private RadioButton zeroTipRadioButton;

    @FXML
    private RadioButton tenTipRadioButton;

    @FXML
    private CheckBox roundCheckBox;

    @FXML
    private ComboBox<String> currencyComboBox;

    @FXML
    private TextField tipField;

    @FXML
    private TextField finalTotalField;

    @FXML
    private Label errorLabel;

    @FXML
    private AnchorPane additionalRubAnchorPane;

    @FXML
    private TextField tipRubField;

    @FXML
    private TextField finalTotalRubField;

    @FXML
    private Button clearButton;

    @FXML
    private javafx.scene.control.Button closeButton;

    @FXML
    private javafx.scene.control.Button minimizeButton;

    /**
     * Initializes default values.
     */
    @FXML
    void initialize() {
        zeroTipRadioButton.setToggleGroup(tip);
        zeroTipRadioButton.setSelected(true);

        fiveTipRadioButton.setToggleGroup(tip);

        tenTipRadioButton.setToggleGroup(tip);

        currencyComboBox.getItems().addAll(currency);
        currencyComboBox.setValue(currency.get(2));
    }

    /**
     * Closes the application's window.
     */
    @FXML
    void close() {
        ((Stage) closeButton.getScene().getWindow()).close();
    }

    /**
     * Minimizes the application's window.
     */
    @FXML
    void minimize() {
        ((Stage) minimizeButton.getScene().getWindow()).setIconified(true);
    }

    /**
     * Gets cursor's coordinates and saves values to {@code xOffset} and {@code yOffset}.
     *
     * @param event cursor's coordinates.
     */
    @FXML
    void pressed(MouseEvent event) {
        xOffset = event.getSceneX();
        yOffset = event.getSceneY();
    }

    /**
     * Moves the application's window by cursor.
     *
     * @param event cursor's coordinates.
     */
    @FXML
    void dragged(MouseEvent event) {
        ((Node)event.getSource()).getScene().getWindow().setX(event.getScreenX() - xOffset);
        ((Node)event.getSource()).getScene().getWindow().setY(event.getScreenY() - yOffset);
    }

    /**
     * Clears all fields, sets {@code clearButton} and {@code errorLabel} visible to false.
     */
    @FXML
    void clear() {
        totalField.clear();
        tipField.clear();
        finalTotalField.clear();
        tipRubField.clear();
        finalTotalRubField.clear();
        errorLabel.setVisible(false);
        clearButton.setVisible(false);
    }

    /**
     * Sets {@code additionalRubAnchorPane} visible to true if {@code currencyComboBox} != RUB.
     * Calls checkInput method.
     */
    @FXML
    void showAdditionalWindow() {
        additionalRubAnchorPane.setVisible(!currencyComboBox.getValue().equals(currency.get(2)));

        checkInput();
    }

    /**
     * Checks {@code totalField} value.
     * If detect exception shows {@code errorLabel} and {@code clearButton}.
     */
    @FXML
    void checkInput() {
        errorLabel.setVisible(false);

        if(totalField.getText().isEmpty()) {
            tipField.clear();
            finalTotalField.clear();
            tipRubField.clear();
            finalTotalRubField.clear();
            errorLabel.setVisible(false);
            clearButton.setVisible(false);
            return;
        }

        double value;

        try {
            value = Double.parseDouble(totalField.getText());
        }
        catch(NumberFormatException NaN) {
            errorLabel.setText("Incorrect input!");
            errorLabel.setVisible(true);
            clearButton.setVisible(true);
            return;
        }

        if(value < 0) {
            errorLabel.setText("Total cannot be less than 0!");
            errorLabel.setVisible(true);
            clearButton.setVisible(true);
        }
        else {
            clearButton.setVisible(true);
            calculate(value);
        }
    }

    /**
     * Calculates {@code tipValue} and {@code finalTotalValue}.
     * If {@code roundCheckBox} is selected rounds {@code finalTotalValue} to up and if {@code zeroTipRadioButton} is unpicked.
     * Sets results to {@code tipField} and {@code finalTotalField}.
     * If {@code currencyComboBox} != RUB calls calculateRUB method.
     *
     * @param value {@code totalField} value.
     */
    @FXML
    private void calculate(double value) {
        double tipValue = 0;
        double finalTotalValue;

        if(tip.getSelectedToggle() != zeroTipRadioButton) {
            tipValue = value / 100 * ((tip.getSelectedToggle() == fiveTipRadioButton) ? 5 : 10);
        }

        finalTotalValue = value + tipValue;

        if(roundCheckBox.isSelected()) {
            finalTotalValue = (tipValue == 0) ? Math.ceil(finalTotalValue) : Math.floor(finalTotalValue);
        }

        tipField.setText(String.format("%.2f", tipValue));
        finalTotalField.setText(String.format("%.2f", finalTotalValue));

        if(!currencyComboBox.getValue().equals(currency.get(2))) calculateRUB(tipValue, finalTotalValue);
    }

    /**
     * Converts {@code tipValue} and {@code finalTotalValue}, saves results to {@code tipRubValue} and {@code finalTotalRubValue}.
     * If {@code roundCheckBox} is selected rounds {@code finalTotalRubValue} to up and if {@code zeroTipRadioButton} is unpicked.
     * Sets results to {@code tipRubField} and {@code finalTotalRubField}.
     *
     * @param tipValue {@code tipField} value.
     * @param finalTotalValue {@code finalTotalField} value.
     */
    private void calculateRUB(double tipValue, double finalTotalValue) {
        double tipRubValue;
        double finalTotalRubValue;

        tipRubValue = tipValue * ((currencyComboBox.getValue().equals(currency.get(0))) ? 75 : 80);
        finalTotalRubValue = finalTotalValue * ((currencyComboBox.getValue().equals(currency.get(0))) ? 75 : 80);

        if(roundCheckBox.isSelected()) {
            finalTotalRubValue = (tipRubValue == 0) ? Math.ceil(finalTotalRubValue) : Math.floor(finalTotalRubValue);
        }

        tipRubField.setText(String.format("%.2f", tipRubValue));
        finalTotalRubField.setText(String.format("%.2f", finalTotalRubValue));
    }
}